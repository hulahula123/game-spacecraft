

using UnityEngine;
using UnityEngine.SceneManagement;


public class SceneTransition : MonoBehaviour
{
    public string scene = "<Insert scene name>";
    public selectSpaceCraft sl;
    public void PerformTransition()
    {
        data dt;
        if (GameObject.FindGameObjectWithTag("data"))
        {
            dt = GameObject.FindGameObjectWithTag("data").GetComponent<data>();

            if (dt && sl)
            {
                dt.spaceShip = sl.index;
                dt.level = 1;
                dt.score = 0;
            }
        }
        SceneManager.LoadScene(scene);
    }
}

