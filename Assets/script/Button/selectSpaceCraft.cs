﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class selectSpaceCraft : MonoBehaviour {

	public List<GameObject> characterList;
	public string[] characterNames;
	public int index=0;
	float move=0.01f;
	int count =0;
	int direction=1;

	public Text characterNameBox;


	// Use this for initialization
	void Start () {
		GameObject[] characters = Resources.LoadAll<GameObject> ("spaceship");
		foreach (GameObject c in characters) {
			GameObject _char = Instantiate(c) as GameObject;
			_char.transform.SetParent (GameObject.Find ("CharacterList").transform);

			characterList.Add (_char);
			_char.SetActive (false);
			characterNameBox.text = characterNames [index];
			characterList [index].SetActive (true);
		}

		foreach (GameObject cha in characterList) {
			cha.transform.position = new Vector3(0,1,0);
            cha.GetComponent<playerController>().enabled = false;
			cha.transform.localScale -= new Vector3(0.5f, 0.5f, 0.5f);
		}
	}
	
	// Update is called once per frame
	void Update () {
		foreach (GameObject a in characterList) {
			a.transform.Translate (0,direction* move, 0);
			count++;
			if (count == 100) {
				direction *= -1;
				count = 0;
			}
		}
		
	}
	public void Next(){
		characterList [index].SetActive (false);
		if (index == characterList.Count - 1) {
			index = 0;
		} else {
			index++;
		}
		characterNameBox.text = characterNames [index];
		characterList [index].SetActive (true);

		//SceneManager.LoadScene ("mainScene");
	}

	public void Previous(){
		characterList [index].SetActive (false);
		if (index == 0) {
			index = characterList.Count - 1;
		} else {
			index--;
		}
		characterNameBox.text = characterNames [index];
		characterList [index].SetActive (true);
	}
}
