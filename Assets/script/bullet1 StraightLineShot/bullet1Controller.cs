﻿using UnityEngine;
using System.Collections;

public class bullet1Controller : MonoBehaviour {

	public GameObject bullet;
    GameObject parent;

    public float timetowait;
    public bool isShootForward;
    public float startX;
    public float startY;
    public float damage;
    public float speed;
    float x;
    float y;
	// Use this for initialization
	void Start () {
        parent = transform.parent.gameObject;
        x = parent.transform.position.x;
        y = parent.transform.position.y;
        if (isShootForward)
            bullet.GetComponent<bullet1Fire>().direction = 1;
        else
            bullet.GetComponent<bullet1Fire>().direction = -1;
        bullet.GetComponent<bullet1Fire>().damage = this.damage;
        bullet.GetComponent<bullet1Fire>().speed = this.speed;
        StartCoroutine ("spawnBullet");

	}

    void Update()
    {
        x = parent.transform.position.x;
        y = parent.transform.position.y;
    }


	IEnumerator spawnBullet()
	{
		while (true) {
			yield return new WaitForSeconds (timetowait);
			Instantiate (bullet, new Vector2 (x + startX, y + startY), Quaternion.identity);
		}
	}

}
