﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class ControlScene3 : MonoBehaviour {

    public Text scoreText;
    public float score;

	// Use this for initialization
	void Start () {
        score = 0;
        scoreText.text = score.ToString();
        //StartCoroutine("changeScene");

	}
	
	// Update is called once per frame
	void Update () {
        score = Convert.ToSingle(scoreText.text);
	}

    IEnumerator changeScene()
    {
        yield return new WaitForSeconds(60);
        SceneManager.LoadScene("selectShip");
    }

}
