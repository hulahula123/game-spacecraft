﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class ControlScene2 : MonoBehaviour {

    public Text scoreText;
    public float score;
    public float time;

	// Use this for initialization
	void Start () {
        score = 0;
        scoreText.text = score.ToString();
        StartCoroutine("changeScene");

	}
	
	// Update is called once per frame
	void Update () {
        score = Convert.ToSingle(scoreText.text);
	}

    IEnumerator changeScene()
    {
        yield return new WaitForSeconds(time);
        //yield return new WaitForSeconds(55);
        SceneManager.LoadScene("Stage3Final");
    }

}
