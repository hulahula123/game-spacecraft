﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class spawnItem : MonoBehaviour {

    System.Random rand;
    public Transform[] spawnPosition;
    public GameObject[] allItem;
    float score = 0;
    bool isSpawn;
    float refScore;

	// Use this for initialization
	void Start () {
        rand = new System.Random();
        isSpawn = true;
        spawnPosition = gameObject.GetComponentsInChildren<Transform>();
        refScore = 0;
	}
	
	// Update is called once per frame
	void Update () {
        score = GameObject.FindGameObjectWithTag("data").GetComponent<data>().score;
        if (score != refScore)
        {
            isSpawn = true;
            refScore = score;
        }

        if (score % 160 == 0 && isSpawn)
        {
            isSpawn = false;
            // ignore first index (main)
            int index = rand.Next(1, spawnPosition.Length);
            Vector3 position = spawnPosition[index].position;
            // ignore ulti
            int itemindex = rand.Next(0, allItem.Length - 1);
            Instantiate(allItem[itemindex], position, Quaternion.identity);
        }
	}
}
