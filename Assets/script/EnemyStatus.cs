﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EnemyStatus : MonoBehaviour {

    public AudioClip dieSound;
    System.Random rand;
    public float HP;
    public GameObject explode;
    public int explodeTimes;
    public float scoreWhenDie;
    public int minRangeExplodex;
    public int maxRangeExplodex;
    public int minRangeExplodey;
    public int maxRangeExplodey;
    public bool isBoss;

    public bool isDie;
    Text scoreText;

    // Use this for initialization
    void Start () {
        rand = new System.Random();
        isDie = false;
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    public void GetHit(float damage)
    {
        HP -= damage;
        if (HP <= 0)
            StartCoroutine(Die());
    }

    IEnumerator Die()
    {
        isDie = true;

        data dt = GameObject.FindGameObjectWithTag("data").GetComponent<data>();
        dt.score += scoreWhenDie;

        gameObject.GetComponent<BoxCollider2D>().enabled = false;
        scoreText = GameObject.FindGameObjectWithTag("scoreText").GetComponent<Text>();
        scoreText.text = dt.score.ToString();
        for (int i = 0; i < explodeTimes; i++)
        {
            float x = Convert.ToSingle(rand.Next(minRangeExplodex, maxRangeExplodex)) + transform.position.x;
            float y = Convert.ToSingle(rand.Next(minRangeExplodey, maxRangeExplodey)) + transform.position.y;
            Vector2 myPosition = new Vector2(x, y);
            yield return new WaitForSeconds(0.2f);
            GetComponent<AudioSource>().PlayOneShot(dieSound, 0.5f);
            Instantiate(explode, myPosition, Quaternion.identity);
        }


        if (isBoss)
        {
            SceneManager.LoadScene("startPage");
        }
        
        Destroy(this.gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "walldown")
            Destroy(this.gameObject);
        
    }
}
