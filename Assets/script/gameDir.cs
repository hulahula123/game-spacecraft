﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameDir : MonoBehaviour {
    
	GameObject HPBar;
	GameObject Ulti1, Ulti2;
	public int lifePoint = 11;
	public int level = 1;
	public int ultimate = 0;
	public Sprite hp10;
	public Sprite hp09;
    public Sprite hp08;
    public Sprite hp07;
    public Sprite hp06;
    public Sprite hp05;
    public Sprite hp04;
    public Sprite hp03;
    public Sprite hp02;
    public Sprite hp01;
    public Sprite hp00;
    public Sprite zerohp;



    public void getHealth (){
		if (lifePoint < 11) {
			lifePoint++;
		} else {
			//do nothing
		}
	}

	public void getBomb(){
		if (lifePoint > 0) {
			lifePoint--;
		}

        if (lifePoint <= 0)
        {
            Die();
        }
	}

    void Die()
    {
        SceneManager.LoadScene("selectShip");
    }

	public void getPowerUp(){
		level++;
        GameObject.FindGameObjectWithTag("data").GetComponent<data>().level++;
	}

	public void getUltimate(){
		ultimate++;
	}
	// Use this for initialization
	void Start () {
		HPBar = GameObject.Find ("HPBar");
		Ulti1 = GameObject.Find ("ulti1");
		Ulti2 = GameObject.Find ("ulti2");
	}

	// Update is called once per frame
	void Update () {
        if (lifePoint == 11)
        {
            HPBar.GetComponent<Image>().sprite = hp10;
        }
        if (lifePoint == 10)
        {
            HPBar.GetComponent<Image>().sprite = hp09;
        }
        if (lifePoint == 9)
        {
            HPBar.GetComponent<Image>().sprite = hp08;
        }
        if (lifePoint == 8)
        {
            HPBar.GetComponent<Image>().sprite = hp07;
        }
        if (lifePoint == 7)
        {
            HPBar.GetComponent<Image>().sprite = hp06;
        }
        if (lifePoint == 6)
        {
            HPBar.GetComponent<Image>().sprite = hp05;
        }
        if (lifePoint == 5)
        {
            HPBar.GetComponent<Image>().sprite = hp04;
        }
        if (lifePoint == 4)
        {
            HPBar.GetComponent<Image>().sprite = hp03;
        }
        if (lifePoint == 3) {
			HPBar.GetComponent<Image>().sprite = hp02;
		}
		if (lifePoint == 2) {
			HPBar.GetComponent<Image>().sprite = hp01;
		}
		if (lifePoint == 1) {
			HPBar.GetComponent<Image>().sprite = hp00;
		}
		if (lifePoint == 0) {
			HPBar.GetComponent<Image> ().sprite = zerohp;
		}

		if (ultimate == 0) {
			Ulti1.SetActive (false);
			Ulti2.SetActive (false);
		}
		if (ultimate == 1) {
			Ulti1.SetActive (true);
			Ulti2.SetActive (false);
		}
		if (ultimate == 2) {
			Ulti1.SetActive (true);
			Ulti2.SetActive (true);
		} 	
	}
}
