﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemybullet1Controller : MonoBehaviour {

    public GameObject bullet;
    public float timetowait;
    public int timestostop;

	// Use this for initialization
	void Start () {
        StartCoroutine(shoot());
	}
	
	IEnumerator shoot()
    {
        for (int i = 0; i < timestostop; i++)
        {
            yield return new WaitForSeconds(timetowait);
            Instantiate(bullet, transform.position, Quaternion.identity);
        }
    }
}
