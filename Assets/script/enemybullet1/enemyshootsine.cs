﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyshootsine : MonoBehaviour {

    public float speed;
    public float direction;
    public float phase;
    public float amplitude;
    public float frequency;

    public void initialize(float sp, float amp, float freq, float phs, float dmg)
    {
        speed = sp;
        amplitude = amp;
        frequency = freq;
        phase = phs;
       
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate((amplitude * Mathf.Sin(2 * Mathf.PI * frequency * transform.position.y + phase) * speed), speed * direction, 0);
        //transform.Translate(0, speed * direction, 0);

    }
}
