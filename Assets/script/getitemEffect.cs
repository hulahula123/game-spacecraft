﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class getitemEffect : MonoBehaviour {

    public float time;
    public Color cr;

	// Use this for initialization
	void Start () {
        gameObject.GetComponent<SpriteRenderer>().color = cr;
        Destroy(gameObject, time);
	}
	
}
