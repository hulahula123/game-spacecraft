﻿using UnityEngine;
using System.Collections;

public class bullet4Controller : MonoBehaviour {

    public GameObject bullet;
    GameObject parent;
    public AudioClip shootSound;

    int level;

    public float timetowait;
    public float startX;
    public float startY;
    public float distanceBetweenWings;
    public float damage;
    public float speed;
    float x;
    float y;
    bool isleftSpawn;
    // Use this for initialization
    void Start()
    {
        parent = transform.parent.gameObject;
        level = parent.GetComponent<gameDir>().level;
        x = parent.transform.position.x;
        y = parent.transform.position.y;
        StartCoroutine("spawnBullet");
        
        bullet.GetComponent<bullet4HomingMissile>().damage = this.damage;
        bullet.GetComponent<bullet4HomingMissile>().speed = this.speed;
        //bullet.GetComponent<Transform>().localScale += new Vector3(level / 5, level / 5, level / 5);
    }

    void Update()
    {
        x = parent.transform.position.x;
        y = parent.transform.position.y;
        level = parent.GetComponent<gameDir>().level;
        
        //bullet.GetComponent<Transform>().localScale += new Vector3(level/5, level/5, level/5);
    }


    IEnumerator spawnBullet()
    {
        while (true)
        {
            float newtime = timetowait - (level * 0.1f);
            if (newtime < 0)
                newtime = 0.05f;
            yield return new WaitForSeconds(newtime);
            GetComponent<AudioSource>().PlayOneShot(shootSound,0.3f);
            if (isleftSpawn)
            {
                Instantiate(bullet, new Vector2(x + startX + (distanceBetweenWings / 2), y + startY), Quaternion.identity);
                isleftSpawn = !isleftSpawn;
            }
            else
            {
                Instantiate(bullet, new Vector2(x + startX - (distanceBetweenWings / 2), y + startY), Quaternion.identity);
                isleftSpawn = !isleftSpawn;
            }
        }
    }
}
