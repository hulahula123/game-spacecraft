﻿using UnityEngine;
using System.Collections;

public class ExplodeTime : MonoBehaviour {

    public float timeInterval;

	// Use this for initialization
	void Start () {
        StartCoroutine(TimeTowaitDestroy(timeInterval));
	}
	
	IEnumerator TimeTowaitDestroy(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(this.gameObject);
    }
}
