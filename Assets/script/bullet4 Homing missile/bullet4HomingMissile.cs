﻿using UnityEngine;
using System.Collections;

public class bullet4HomingMissile : MonoBehaviour {

    public float speed;
    public float damage;
    GameObject[] trackingEnemy;
    public float timeDestroy;

    public GameObject explode;
    // Use this for initialization
    void Start()
    {
        trackingEnemy = GameObject.FindGameObjectsWithTag("enemy");
        Destroy(gameObject, timeDestroy);
    }

    // Update is called once per frame
    void Update()
    {
        trackingEnemy = GameObject.FindGameObjectsWithTag("enemy");
        if (trackingEnemy.Length != 0)
        {
            float xMs = transform.position.x;
            float yMs = transform.position.y;
            // find min distance
            int enPos = 0;
            float mindistance = euclidianDistance(Mathf.Abs(xMs - trackingEnemy[0].transform.position.x), Mathf.Abs(yMs - trackingEnemy[0].transform.position.y));
            for (int i = 0; i < trackingEnemy.Length; i++)
            {
                float xEn = trackingEnemy[0].transform.position.x;
                float yEn = trackingEnemy[0].transform.position.y;
                float refDistance = euclidianDistance(Mathf.Abs(xMs - xEn), Mathf.Abs(yMs - yEn));
                if (refDistance < mindistance)
                {
                    enPos = i;
                    mindistance = refDistance;
                }
            }
            Vector3 dir = trackingEnemy[enPos].transform.position - transform.position;
            float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

            transform.Translate(-1 * transform.up * speed);
        }
        else
        {
            transform.Translate(0, speed, 0);
        }
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "wall")
            Destroy(this.gameObject);

        if (other.tag == "enemy")
        {
            other.gameObject.GetComponent<EnemyStatus>().GetHit(this.damage);

            Destroy(this.gameObject);
            Instantiate(explode, transform.position, Quaternion.identity);
        }

        if (other.tag == "meteor")

        {
            Destroy(this.gameObject);
            Instantiate(explode, transform.position, Quaternion.identity);
        }
    }

    private float euclidianDistance(float x, float y)
    {
        return Mathf.Sqrt(Mathf.Pow(x, 2) + Mathf.Pow(y, 2));
    }
}
