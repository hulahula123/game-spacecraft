﻿using UnityEngine;
using System.Collections;

public class bgSlide : MonoBehaviour {

    public int height;
    public float speed;
	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(0, -speed, 0);
        if (transform.position.y < -1 * height)
        {
            transform.Translate(0, height * 2, 0);
        }
	}
}
