﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RandomSpawnEnemy : MonoBehaviour {

    public GameObject[] allSpawnPattern;
    public float time;
    System.Random rand;

	// Use this for initialization
	void Start () {
        rand = new System.Random();
        StartCoroutine("spawn");
	}
	
	IEnumerator spawn()
    {
        while(true)
        {
            yield return new WaitForSeconds(time);
            int index = rand.Next(0, allSpawnPattern.Length);
            Instantiate(allSpawnPattern[index]);
        }
    }
}
