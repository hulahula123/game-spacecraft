﻿using UnityEngine;
using System.Collections;

public class bullet2FireLeft : MonoBehaviour {

	public float speed;
    public float direction;
    public float damage;
    public GameObject explode;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(-speed / 2, speed * direction, 0);

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "wall")
            Destroy(this.gameObject);

        if(other.tag == "enemy")
        {
            other.gameObject.GetComponent<EnemyStatus>().GetHit(this.damage);

            Destroy(this.gameObject);
            Instantiate(explode, transform.position, Quaternion.identity);
        }
        if (other.tag == "meteor")

        {
            Destroy(this.gameObject);
            Instantiate(explode, transform.position, Quaternion.identity);
        }
    }
}
