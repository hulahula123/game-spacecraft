﻿using UnityEngine;
using System.Collections;

public class bullet2TriangleShot : MonoBehaviour {

    public AudioClip shootSound;
    public GameObject bulletLeft;
    public GameObject bulletCenter;
    public GameObject bulletRight;
    GameObject parent;

    public float timetoSpawn;
    public bool isShootForward;
    public float startX;
    public float startY;
    public float speed;
    public float damage;

    int level;

    float x;
    float y;
    // Use this for initialization
    void Start()
    {
        parent = transform.parent.gameObject;
        level = parent.GetComponent<gameDir>().level;
        x = parent.transform.position.x;
        y = parent.transform.position.y;

        if (isShootForward)
        {
            bulletLeft.GetComponent<bullet2FireLeft>().direction = 1;
            bulletCenter.GetComponent<bullet2FireCenter>().direction = 1;
            bulletRight.GetComponent<bullet2FireRight>().direction = 1;
        }
        else
        {
            bulletLeft.GetComponent<bullet2FireLeft>().direction = -1;
            bulletCenter.GetComponent<bullet2FireCenter>().direction = -1;
            bulletRight.GetComponent<bullet2FireRight>().direction = -1;
        }

        bulletLeft.GetComponent<bullet2FireLeft>().damage = this.damage;
        bulletCenter.GetComponent<bullet2FireCenter>().damage = this.damage;
        bulletRight.GetComponent<bullet2FireRight>().damage = this.damage;

        bulletLeft.GetComponent<bullet2FireLeft>().speed = this.speed;
        bulletCenter.GetComponent<bullet2FireCenter>().speed = this.speed;
        bulletRight.GetComponent<bullet2FireRight>().speed = this.speed;



        StartCoroutine("spawnBullet");
    }

    void Update()
    {
        x = parent.transform.position.x;
        y = parent.transform.position.y;
        level = parent.GetComponent<gameDir>().level;
    }
    IEnumerator spawnBullet()
    {
        while (true)
        {
            float newtime = timetoSpawn - (level * 0.02f);
            if (newtime < 0)
                newtime = 0.01f;
            yield return new WaitForSeconds(newtime);
            GetComponent<AudioSource>().PlayOneShot(shootSound, 0.13f);
            Instantiate(bulletLeft, new Vector2(x + startX, y + startY), Quaternion.identity);
            Instantiate(bulletCenter, new Vector2(x + startX, y + startY), Quaternion.identity);
            Instantiate(bulletRight, new Vector2(x + startX, y + startY), Quaternion.identity);
        }
    }
}
