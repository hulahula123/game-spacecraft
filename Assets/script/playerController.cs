﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class playerController : MonoBehaviour
{
    public int index;
    gameDir director;
    Animator animator;
    public GameObject itemgetEffect;
    public AudioClip getItem;
    public AudioClip getHit;
    AudioSource audioSource;
    public float speed;
    bool isLeftWall;
    bool isRightWall;
    bool isDownWall;

    public Vector3 refMouse;

    // Use this for initialization
    void Start()
    {
        director = gameObject.GetComponent<gameDir>();
        audioSource = GetComponent<AudioSource>();
        animator = gameObject.GetComponent<Animator>();
        if (GameObject.FindGameObjectWithTag("data"))
        {
            data dt = GameObject.FindGameObjectWithTag("data").GetComponent<data>();
            if (index != dt.spaceShip)
            {
                this.gameObject.SetActive(false);
            }
            director.level = dt.level;
        }

        isLeftWall = false;
        isRightWall = false;
        isDownWall = false;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "bomb" || other.gameObject.tag == "enemybullet")
        {
            director.getBomb();
            audioSource.PlayOneShot(getItem);
            Destroy(other.gameObject);
        }
        else if (other.gameObject.tag == "hp")
        {
            itemgetEffect.GetComponent<getitemEffect>().cr = Color.red;
            Instantiate(itemgetEffect, transform.position, Quaternion.identity);
            director.getHealth();
            audioSource.PlayOneShot(getItem);
            Destroy(other.gameObject);
        }
        else if (other.gameObject.tag == "levelup")
        {
            itemgetEffect.GetComponent<getitemEffect>().cr = Color.green;
            Instantiate(itemgetEffect, transform.position, Quaternion.identity);
            director.getPowerUp();
            audioSource.PlayOneShot(getItem);
            Destroy(other.gameObject);
        }
        else if (other.gameObject.tag == "ulti")
        {
            itemgetEffect.GetComponent<getitemEffect>().cr = Color.yellow;
            Instantiate(itemgetEffect, transform.position, Quaternion.identity);
            director.getUltimate();
            audioSource.PlayOneShot(getItem);
            Destroy(other.gameObject);
        }
        else if (other.gameObject.tag == "enemy" || other.gameObject.tag == "meteor")
        {
            director.getBomb();
            audioSource.PlayOneShot(getHit);
            other.GetComponent<EnemyStatus>().GetHit(30);
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {

        // wall left
        if (other.gameObject.tag == "wallleft")
        {
            isLeftWall = true;
        }
        // wall right
        if (other.gameObject.tag == "wallright")
        {
            isRightWall = true;
        }
        // wall down
        if (other.gameObject.tag == "walldown")
        {
            isDownWall = true;
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        // wall left
        if (other.gameObject.tag == "wallleft")
        {
            isLeftWall = false;
        }
        // wall right
        if (other.gameObject.tag == "wallright")
        {
            isRightWall = false;
        }
        // wall down
        if (other.gameObject.tag == "walldown")
        {
            isDownWall = false;
        }
    }
    // Update is called once per frame
    void Update()
    {
        float x = 0;
        float y = 0;
        int keyHorizontal = 0;
        int keyVertical = 0;
        if (Input.GetKey(KeyCode.RightArrow))
            keyHorizontal = 1;
        if (Input.GetKey(KeyCode.LeftArrow))
            keyHorizontal = -1;
        if (Input.GetKey(KeyCode.UpArrow))
            keyVertical = 1;
        if (Input.GetKey(KeyCode.DownArrow))
            keyVertical = -1;

        if (Input.GetMouseButtonUp(0))
        {
            refMouse = Vector3.zero;
        }

        if (Input.GetMouseButton(0))
        {
            var v3 = Input.mousePosition;
            v3.z = 10.0f;
            v3 = Camera.main.ScreenToWorldPoint(v3);

            if (refMouse == null || refMouse == Vector3.zero)
            {
                refMouse = v3;
            }

            if (v3 != refMouse)
            {
                x = v3.x - refMouse.x;
                y = v3.y - refMouse.y;

                if (x < 0)
                    keyHorizontal = -1;
                else if (x > 0)
                    keyHorizontal = 1;
                else if (x == 0)
                    keyHorizontal = 0;

                if (y < 0)
                    keyVertical = -1;
                else if (y > 0)
                    keyVertical = 1;
                else if (y == 0)
                    keyVertical = 0;

                refMouse = v3;
            }
        }

        if (keyHorizontal != 0)
        {
            transform.localScale = new Vector3(-keyHorizontal, 1, 1);
            // when touch left wall and right wall
            if (keyHorizontal == 1 && isRightWall)
            {
                // is touch right wall and move right
            }
            else if (keyHorizontal == -1 && isLeftWall)

            {
                // is touch left wall and move left
            }
            else
            {
                //transform.Translate(new Vector3(speed * keyHorizontal, 0, 0));
                transform.Translate(x, y * 0.3f, 0);
            }


            animator.SetBool("isLeft", true);
        }
        else
        {
            animator.SetBool("isLeft", false);
        }


        // up down
        if (keyVertical != 0)
        {
            if (keyVertical == 1)
            {
                animator.SetBool("isForward", true);
            }


            // down when touch down wall
            if (keyVertical == -1 && isDownWall)
            {
                // not translate
            }
            else
            {
                transform.Translate(new Vector3(0, keyVertical * speed, 0));
            }
        }
        else
        {
            animator.SetBool("isForward", false);
        }


    }
}
