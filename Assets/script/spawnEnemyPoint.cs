﻿using UnityEngine;
using System.Collections;

public class spawnEnemyPoint : MonoBehaviour {

    public GameObject enemyToSpawn;
    public float waitbeforeStart;
    public float timeToWaitSpawn;
    public int timesToStop;

	// Use this for initialization
	void Start () {
        StartCoroutine(spawn());
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    IEnumerator spawn()
    {
        int count = 0;
        yield return new WaitForSeconds(waitbeforeStart);
        while (timesToStop > count)
        {
            yield return new WaitForSeconds(timeToWaitSpawn);
            Instantiate(enemyToSpawn, transform.position, Quaternion.identity);
            count++;
        }
    }
}
