﻿using UnityEngine;
using System.Collections;

public class bullet3Controller : MonoBehaviour {

    public AudioClip shootSound;
    public GameObject bullet;
    GameObject parent;

    public float timetowait;
    public bool isShootForward;
    public float startX;
    public float startY;
    public float amplitude;
    public float frequency;
    public float phase;
    public float speed;
    public float damage;

    int level;

    public Color leftBulletColor;
    public Color rightBulletColor;

    float x;
    float y;
    
    // Use this for initialization
    void Start () {
        parent = transform.parent.gameObject;
        level = parent.GetComponent<gameDir>().level;
        x = parent.transform.position.x;
        y = parent.transform.position.y;
        if (isShootForward)
            bullet.GetComponent<bullet3FireSine>().direction = 1;
        else
            bullet.GetComponent<bullet3FireSine>().direction = -1;
        bullet.GetComponent<bullet3FireSine>().initialize(speed, amplitude, frequency, phase, damage);


        StartCoroutine("spawnBullet");
    }
	
	// Update is called once per frame
	void Update ()
    {
        x = parent.transform.position.x;
        y = parent.transform.position.y;
        level = parent.GetComponent<gameDir>().level;
    }

    IEnumerator spawnBullet()
    {
        while (true)
        {
            float newtime = timetowait - (level * 0.02f);
            if (newtime < 0)
                newtime = 0.01f;
            yield return new WaitForSeconds(newtime);
            GetComponent<AudioSource>().PlayOneShot(shootSound, 0.13f);
            bullet.GetComponent<bullet3FireSine>().initialize(speed, amplitude, frequency, phase, damage);
            bullet.GetComponent<SpriteRenderer>().color = leftBulletColor;
            Instantiate(bullet, new Vector2(x + startX, y + startY), Quaternion.identity);
            //bullet.GetComponent<bullet3FireSine>().initialize(speed, -amplitude, frequency, phase, damage);
            //bullet.GetComponent<SpriteRenderer>().color = rightBulletColor;
            //Instantiate(bullet, new Vector2(x + startX, y + startY), Quaternion.identity);
        }
    }


}
