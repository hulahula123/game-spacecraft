﻿using UnityEngine;
using System.Collections;

public class bullet3FireSine : MonoBehaviour {

    public float speed;
    public float direction;
    public float phase;
    public float amplitude;
    public float frequency;
    public float damage;
    public GameObject explode;

    public void initialize(float sp, float amp, float freq, float phs, float dmg)
    {
        speed = sp;
        amplitude = amp;
        frequency = freq;
        phase = phs;
        damage = dmg;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate((amplitude* Mathf.Sin(2 * Mathf.PI * frequency * transform.position.y + phase) * speed), speed * direction, 0);
        //transform.Translate(0, speed * direction, 0);

    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "wall")
            Destroy(this.gameObject);

        if (other.tag == "enemy")
        {
            other.gameObject.GetComponent<EnemyStatus>().GetHit(this.damage);

            Destroy(this.gameObject);
            Instantiate(explode, transform.position, Quaternion.identity);
        }
        if (other.tag == "meteor")

        {
            Destroy(this.gameObject);
            Instantiate(explode, transform.position, Quaternion.identity);
        }
    }
}
