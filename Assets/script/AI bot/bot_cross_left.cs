﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bot_cross_left : MonoBehaviour {

    public float speed;
    public float direction;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        transform.Translate(-speed / 2, direction * speed, 0);

    }
}
