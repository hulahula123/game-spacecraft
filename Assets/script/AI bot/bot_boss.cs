﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bot_boss : MonoBehaviour {

    public float speed;
    public float direction;
    int count = 0;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        
        transform.Translate(direction * speed, 0, 0);
        count++; 
        if (count < 100)
        {
            transform.Translate(0, -speed, 0);
        }
    }
    public void ChangeDirection()
    {
        direction = -direction;
    }

    void OnTriggerEnter2D(Collider2D obj)
    {
        if(obj.gameObject.CompareTag("wallright"))
        {
            ChangeDirection(); 
        }
        if (obj.gameObject.CompareTag("wallleft"))
        {
            ChangeDirection();
        }
    }
}
