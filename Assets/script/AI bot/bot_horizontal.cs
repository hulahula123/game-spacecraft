﻿using UnityEngine;
using System.Collections;

public class bot_horizontal : MonoBehaviour {

	public float speed;
	public float direction;
	public float phase;
	public float amplitude;
	public float frequency;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		transform.Translate((amplitude* Mathf.Sin(2 * Mathf.PI * frequency * transform.position.y + phase) * speed), speed * direction, 0);
		//transform.Translate(0, speed * direction, 0);
	
	}
}
