﻿using UnityEngine;
using System.Collections;

public class bot_cross : MonoBehaviour {

	public float speed;
	public float direction;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		transform.Translate(speed / 2, direction * speed, 0);

	}
}
